Math_StatIndex
==============

Package to calculate statistical index numbers.

An index number is "(a) statistic which assigns a single number to several
individual statistics in order to quantify trends." 
(From [MathWorld's "Index Number"](http://mathworld.wolfram.com/IndexNumber.html)).

This package allows the calculation of some of the better known index
numbers: Bowley index, Fisher index, Geometric mean index, Harmonic mean
index, Laspeyres&apos; index, Marshall-Edgeworth index, Mitchell index,
Paasche's index, and Walsh index.
 
This is old code I wrote circa 2005.

-- Jesus M. Castagnetto
